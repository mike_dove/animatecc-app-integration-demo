#Adobe AnimateCC Function
>**How to Integrate Adobe AnimateCC canvas into a regular app structure.**

AnimateCC is the new version of Flash that exports to `<canvas>` objects rather than `.swf`.

This guide deals with how to make use of the exported files in a live site environment.

Upon publishing from CC you will find two files

_(where "aniName" is the orignal source file)_

1. **`aniName.html`** - contains the canvas object and a singular Js function used to initiate the animation.
2.  **`aniName.js`** - contains the animations parameters, constructors and timeline.

In the the `aniName.html` you will find a function tor run the animation.

```javascript
var canvas, stage, exportRoot;
function init() {

	canvas = document.getElementById("canvas");
	exportRoot = new lib.aniname();

	stage = new createjs.Stage(canvas);
	stage.addChild(exportRoot);
	stage.update();

	createjs.Ticker.setFPS(lib.properties.fps);
	createjs.Ticker.addEventListener("tick", stage);
}
```

###The Problems
The issue with this basic code comes when you want to include multiple animations on a page/site.
1. All `<canvas>` objects must be given unique `id`'s
2. All `aniName.js` files must be included as seperate links
3. each `<canvas>` must have its own function

###The Solution
This demo provides the following solutions
1. Canvas objects link to animations via a `data` attribute
2. Animation codes can be combined into a single minimized file `animations.js`
3. A global function checks all `<canvas>` objects and assigns the correct animation to each.

####Step 1 - Prepare
Publish all your animations from CC and copy and paste all the `aniName.js` files contents into a new `animation.js`. This can be minimized if you wish.

Open your sites `index.html` and add the following scripts as links.

```html
<!--Jquery of your choice -->
<script type="text/javascript" src="Jquery"></script>
<!-- CreateJS.min -->
<script src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
<!--Your Sites Main Js File -->
<script type="text/javascript" src="main.js"></script>
<!--Your combined animations -->
<script type="text/javascript" src="animations.js"></script>
```

####Step 2 - Place the Canvas'

In your HTML you will need to place all your <canvas> elements. Each canvas will need to following:

1. `class="aniCC"` to distinguish it from any other canvas elements used on the site for other purposes
2. `data-ani="aniName"` A unique data tag that matches the name of the specific animation you want to place in it.
3. You must also include `width=""` & `height=""` attributes of the `<canvas>`.

```html
<canvas class="aniCC" data-ani="ani1" width="200" height="200"></canvas>
```

####Step 3 - include the function
The following function is designed to be included as part of the Tommy approved `app()`
structure for javascript.

Place the following function in your `main.js` app structure:

```javascript
animateCC:function() { 
	console.log('Running app.animate()');
		
		$('canvas.aniCC').each(function(){ 
				console.log('Getting animation: '+$(this).data('ani'));
			
			var thisAni = $(this).data('ani');
			
			canvas = $(this)[0];
			exportRoot = new lib[thisAni](); 
			
			stage = new createjs.Stage(canvas);
			stage.addChild(exportRoot);
			stage.update();
			
			createjs.Ticker.setFPS(lib.properties.fps);
			createjs.Ticker.addEventListener("tick", stage);
		});
	},
```

You can now initiate the function by running `app.animateCC()` either from your `app.init()` or by binding it to another event.

Basically the function finds all the canvas objects with the correct class of `aniCC`.  For each case it grabs the animation name form the data  tag and assigns it to the current `<canvas>`.

###! ISSUES !
1. Currently no way to separate the properties from each other so all animations will have same properties defined by last included set. If all canvas are the same size and FPS this is fine. _(Please see `unique-properties` branch for latest updates solving this issue)_

---

> Written by **Mike Dove** Feb 2016 –  mike@thisistommy.com
> Special thanks to Farhad and Carlos for JS help.